# External Page payment proxy

## Cosa fa
Questo servizio si occupa di fare da intermediario tra la stanza del cittadino e un qualsiasi portale esterno di pagamento pagopa

### Descrizione
Questo gateway di pagamento, non si occuperà né di creare né di aggiornare pagamenti fatti dall’utente a partire da una pratica, bensì si occuperà semplicemente di redirigere l’utente verso il portale esterno o pagopa in modo che possa pagare e successivamente far ritornare l’utente sulla sua pratica una volta completata l’operazione. L’utente poi fornirà sulla pratica la ricevuta di pagamento e infine l’operatore approverà o rifiuterà in base a tale ricevuta.

### User stories
1. Come admin, voglio poter avere la possibilità di configurare per un servizio un gateway di pagamento “fake”, dove vado ad inserire nel form di configurazione un link statico ad una pagina esterna che sarà poi usata dall’utente per procedere al pagamento
2. Come cittadino voglio poter vedere nello step "Pagamento" della pratica SOLO il bottone “Paga online” per poter pagare nel portale esterno, questa sarà una nuova finestra del browser, l’utente sarà quindi avvisato di ciò mediante modal, dove gli sarà mostrata la causale da usare nel pagamento.
3. Come cittadino voglio poter avere la possibilità, quando clicco sul bottone “Ho pagato” nella modal (il cui form sarà uno schema restituito dal proxy) di inserire un file di ricevuta di avvenuto pagamento, o perlomeno un’evidenza qualsiasi di avvenuto pagamento (IUV, id transazione o altro). I campi dove inserire questi dati non saranno obbligatori e l’utente potrà anche dichiarare di aver pagato senza doverli per forza inserire.
4. Come operatore voglio poter vedere la ricevuta o l’evidenza usata dall’utente per confermare l’avvenuto pagamento ed eventualmente accettare o rifiutare la pratica in base a ciò

### Dettagli implementativi
1. Il link per pagare viene salvato nella configurazione di pagamento per ogni servizio, il quale viene salvato dal payment proxy su un file json (come per gli altri proxy). Il json di conifigurazione del servizio potrebbe essere fatto così
```json
{
  "tenant_id": "b212c4b4-db26-4404-8c7c-47dab99dd2e6",
  "payment_link": "https://payment-gateway.com/payment",
  "category": "9/0101100IM/",
  "reason": "Pagamento Pratiche Polizia Municipale",
  "id": "23d57b65-5eb9-4f0a-a507-fbcf3057b248",
  "active": true
}
```
2. Il json di configurazione del tenant invece potrebbe essere fatto così
```json
{
  "id": "b212c4b4-db26-4404-8c7c-47dab99dd2e6",
  "active": true
}
```
3. Il proxy reindirizzerà l’utente all’url indicata nella configurazione. Il proxy inoltre esporrà un form che verrà compilata dall’utente con i dati del pagamento fatto, e poi userà questi ultimi per creare l’evento di pagamento completato. Il submit sarà protetto da bearer token.


## Stadio di sviluppo
Il servizio è nello stadio di Stable 1.0.4

## Come si usa
Da terminale:
1. `git clone https://gitlab.com/opencontent/stanza-del-cittadino/external-page-payment-proxy.git`
2. `cd external-page-payment-proxy`
3. `docker-compose up -d` (assicurarsi che kafka sia su, in caso contrario digitare `docker-compose up -d kafka`)
4. Accedere alla documentazione delle API via `http://0.0.0.0:8000/docs`
5. Usare la `POST /tenants` e inserire la configurazione usando lo schema indicato
6. Usare la `POST /services` e inserire la configurazione usando lo schema indicato (inserire come tenant_id l'id del tenant precedentemente creato)
7. Accedere a `kafka-ui` via `http://localhost:8080`
8. Accedere al topic `payments` e andare nella sezione Messages
9. Copiare il messaggio presente nel topic
10. Premere "Produce Message",
11. Copiare il messaggio settando
    1. Il campo `id` ad un uuid valido
    2. Il campo `iud` all esadecimale dell'id dello step precedente (basta eliminare i trattini)
    3. Il campo `expire_at` ad una data successiva a quella odierna
    4. Il campo `tenant_id` all'id settato nello step 5
    5. Il campo `service_id` all'id settato nello step 6
12. Inviare il messaggio
13. Accedere alla documentazione delle API via `http://0.0.0.0:8000/docs
14. Usare la GET `/online-payment/{payment_id}` e inserire l'id settato allo step 11.1
15. Procedere al pagamento nella pagina html restituita



## Configurazione
### Configurazione variabili d'ambiente


### Configurazione Tenant
```json
{
  "id": "b212c4b4-db26-4404-8c7c-47dab99dd2e6",
  "active": true
}
```
### Configurazione Servizio
```json
{
  "tenant_id": "b212c4b4-db26-4404-8c7c-47dab99dd2e6",
  "payment_link": "https://payment-gateway.com/payment",
  "category": "9/0101100IM/",
  "reason": "Pagamento Pratiche Polizia Municipale",
  "id": "23d57b65-5eb9-4f0a-a507-fbcf3057b248",
  "active": true
}
```

## Sast

## Dependency Scanning

## Renovate

## Coding Style
