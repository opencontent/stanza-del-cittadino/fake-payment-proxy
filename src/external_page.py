import json
import os
from base64 import b64decode
from enum import Enum
from typing import List, Optional

from oc_python_sdk.models.payment import HttpMethodType, Payment, PaymentStatus
from pydantic import BaseModel

from file_manager import FileManager
from logger import external_page_logger as logger

BASE_PATH_CONFIG = os.environ.get('BASE_PATH_CONFIG', 'tenants/')
BASE_PATH_EVENT = os.environ.get('BASE_PATH_EVENT', 'payments/')
BASE_PATH_RECEIPT = os.environ.get('BASE_PATH_RECEIPT', 'receipts/')
CANCEL_PAYMENT = True if os.environ.get('CANCEL_PAYMENT', 'false').lower() == 'true' else False


class ExternalPageException(Exception):  # noqa: N818
    pass


class ReceiptModel(BaseModel):
    hash: str
    name: str
    originalName: str
    size: int
    storage: str
    type: str
    url: str


class PaymentSummary(BaseModel):
    iuv: Optional[str]
    receipt: List[ReceiptModel]


class PaymentSummaryModel(BaseModel):
    data: PaymentSummary


class LinkType(Enum):
    INTERNAL_LINK = 'internal'
    EXTERNAL_LINK = 'external'


class ExternalPage:
    def __init__(
        self,
        app_online_url,
        app_receipt_url,
        app_landing_url,
        config_path,
    ):
        logger.info('Starting EXTERNAL PAGE initialization')

        self.file_manager = FileManager()
        self.config_path = config_path

        self.tenants = {}
        self.services = {}

        self.app_online_url = app_online_url
        self.app_receipt_url = app_receipt_url
        self.app_landing_url = app_landing_url

        self._load_tenants_and_services()

        logger.info('EXTERNAL PAGE initialization completed')

    def _load_tenants_and_services(self):
        for key, value in self.file_manager.get_all_files(BASE_PATH_CONFIG).items():
            if 'active' not in value or value['active']:
                if 'tenant' in key:
                    self.add_or_update_tenant_conf(value)
                else:
                    self.add_or_update_service_conf(value)
        logger.debug(self.tenants)
        logger.debug(self.services)

    def _create_payment(self, event):
        payment = Payment.parse_obj(event)
        logger.info(f'Event {payment.id} from application {payment.remote_id}: Preparing payment creation request')

        payment_id = str(payment.id)
        payload = self._get_creation_request(payment)
        try:
            logger.debug(
                f'Event {payment.id} from application {payment.remote_id}: '
                f'Sending creation request with data {payload}',
            )

            # Link pagamento online
            payment.links.online_payment_begin.url = self.get_online_url(payment_id, url_type=LinkType.INTERNAL_LINK)
            payment.links.online_payment_begin.method = HttpMethodType.HTTP_METHOD_GET

            # Link per scaricare la ricevuta di pagamento disponibile SOLO a pagamento effettuato # noqa: W505
            payment.links.receipt.url = self._get_receipt_url(payment_id=payment_id)
            payment.links.receipt.method = HttpMethodType.HTTP_METHOD_GET

            payment.status = PaymentStatus.STATUS_PAYMENT_PENDING.value
            payment = payment.update_time('links.update.next_check_at')
        except Exception as ex:
            payment.status = PaymentStatus.STATUS_CREATION_FAILED
            logger.error(f'Event {payment.id} from application {payment.remote_id}: Error: {ex}')

        payment = payment.update_time('updated_at')

        return json.loads(payment.json())

    async def process_payment(self, event, **kwargs):
        payment = Payment(**event)
        if self._is_external(payment):
            logger.info(f"Event {event['id']} from external source: Processing event")
            payment.links.offline_payment.url = None
            payment.links.offline_payment.method = None
            self.file_manager.save_file(path=f'{BASE_PATH_EVENT}{str(payment.id)}.json', data=json.dumps(event))
            return json.loads(payment.json())
        if payment.is_payment_creation_needed():
            logger.info(f"Event {event['id']} from application {event['remote_id']}: Processing event")
            return self._create_payment(event)
        else:
            logger.info(f"Event {event['id']} from application {event['remote_id']}: Processing not required")
            return None

    def _is_external(self, payment):
        """
        :param payment:
        :return: bool
        Checks if a payment was processed by the proxy
        or if it was imported from an external source
        """
        try:
            if payment.status == PaymentStatus.STATUS_PAYMENT_PENDING and not self._payment_exists(str(payment.id)):
                self._get_service_by_identifier(service_id=str(payment.service_id))
                return True
        except ExternalPageException:
            return False
        return False

    def get_online_url(self, payment_id: str, url_type: LinkType = LinkType.EXTERNAL_LINK):
        if url_type == LinkType.INTERNAL_LINK:
            return self.app_online_url.replace('{id}', payment_id)
        payment, tenant = self._get_payment_and_tenant(payment_id=payment_id)
        service = self._get_service_by_identifier(str(payment.service_id))
        landing_url = self.get_landing_url(payment_id=str(payment.id), url_type=LinkType.INTERNAL_LINK)

        payment = payment.update_time('links.online_payment_begin.last_opened_at')
        payment = payment.update_time('updated_at')
        is_paid = payment.status == PaymentStatus.STATUS_COMPLETE
        return service['payment_link'], json.loads(payment.json()), is_paid, landing_url

    def get_landing_url(
        self,
        payment_id: str,
        payment_data: Optional[PaymentSummaryModel] = None,
        url_type: LinkType = LinkType.EXTERNAL_LINK,
    ):
        if url_type == LinkType.INTERNAL_LINK:
            return self.app_landing_url.replace('{id}', payment_id)

        payment, tenant = self._get_payment_and_tenant(payment_id=payment_id)
        payment = payment.update_time('links.online_payment_landing.last_opened_at')
        payment = payment.update_time('updated_at')

        payment.payment.iuv = payment_data.data.iuv
        self.file_manager.save_file(
            path=f'{BASE_PATH_RECEIPT}{payment_id}.pdf',
            data=b64decode(payment_data.data.receipt[0].url.split(',')[1], validate=True),
        )
        payment.status = PaymentStatus.STATUS_COMPLETE

        return payment.links.online_payment_landing.url, json.loads(payment.json())

    def _get_receipt_url(self, payment_id: str):
        return self.app_receipt_url.replace('{id}', payment_id)

    def get_receipt_file(self, payment_id: str):
        payment, tenant = self._get_payment_and_tenant(payment_id=payment_id)

        try:
            receipt_pdf = self.file_manager.get_file(path=f'{BASE_PATH_RECEIPT}{payment_id}.pdf')
            payment = payment.update_time('links.receipt.last_opened_at')
            payment = payment.update_time('updated_at')
            return receipt_pdf, json.loads(payment.json())
        except Exception as ex:
            logger.error(f'Payment {payment.payment.iuv} from application {payment.remote_id}: Error: {ex}')
            raise ExternalPageException('Receipt not available')

    def _get_creation_request(self, payment: Payment):
        logger.debug(f'Event {payment.id} from application {payment.remote_id}: Prepare payload for creation request')

        service = self._get_service_by_identifier(str(payment.service_id))

        return service

    def _get_tenant_by_identifier(self, tenant_id: str):
        if tenant_id in self.tenants:
            return self.tenants[tenant_id]
        logger.error(f'Missing configuration for tenant identifier {tenant_id}')
        raise ExternalPageException('Tenant configuration missing')

    def _get_service_by_identifier(self, service_id: str):
        if service_id in self.services:
            return self.services[service_id]
        logger.error(f'Missing configuration for service identifier {service_id}')
        raise ExternalPageException('Service configuration missing')

    def _get_payment_and_tenant(self, payment_id: str, cancel_payment=False):
        payment = self.file_manager.get_file(path=f'{BASE_PATH_EVENT}{payment_id}.json')
        if not payment:
            raise ExternalPageException(f'Missing payment {payment_id}')
        payment = Payment(**json.loads(payment))
        tenant = self._get_tenant_by_identifier(str(payment.tenant_id))
        return payment, tenant

    def add_or_update_service_conf(self, config):
        self.services[config['id']] = config

    def add_or_update_tenant_conf(self, config):
        self.tenants[config['id']] = config

    def delete_tenant_conf(self, tenant_id):
        del self.tenants[str(tenant_id)]

    def delete_service_conf(self, service_id):
        del self.services[str(service_id)]

    def _payment_exists(self, payment_id):
        return self.file_manager.file_exists(path=f'{BASE_PATH_EVENT}{payment_id}.json')

    def _cancel_payment(self, payment_id):
        logger.warning(f'Canceling payment {payment_id}')
        return {
            'id': payment_id,
            'status': PaymentStatus.STATUS_PAYMENT_FAILED.value,
        }
