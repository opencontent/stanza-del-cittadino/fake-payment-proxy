#!/usr/bin/env python
import os
from contextlib import asynccontextmanager
from uuid import UUID

import uvicorn as uvicorn
from dotenv import load_dotenv
from fastapi import Body, FastAPI, HTTPException, Path, Response, status
from fastapi.responses import HTMLResponse
from pydantic import ValidationError

from __init__ import __version__ as app_version
from configurator import (
    ConfigurationNotAllowed,
    ConfigurationNotFound,
    SchemaNotFound,
    Service,
    ServiceConfigurator,
    ServiceUpdate,
    Tenant,
    TenantConfigurator,
    TenantUpdate,
    get_payment_schema,
    get_service_schema,
    get_tenant_schema,
)
from external_page import ExternalPage, ExternalPageException, PaymentSummaryModel
from logger import proxy_logger as logger
from mykafka import Kafka
from payment_page import get_template

load_dotenv()

logger.info('Starting')
APP_HOST = os.environ.get('APP_HOST', '0.0.0.0')
APP_PORT = os.environ.get('APP_PORT', 8000)
EXTERNAL_API_URL = os.environ.get('EXTERNAL_API_URL', 'http://0.0.0.0:8000')
INTERNAL_API_URL = os.environ.get('INTERNAL_API_URL', 'http://0.0.0.0:8000')
EXTERNAL_PAGE_CONFIG_PATH = os.environ.get('EXTERNAL_PAGE_CONFIG_PATH', '../tenants')
KAFKA_TOPIC_NAME = os.environ.get('KAFKA_TOPIC_NAME', 'payments')
KAFKA_BOOTSTRAP_SERVERS = os.environ.get('KAFKA_BOOTSTRAP_SERVERS', 'localhost:29092').split(',')
KAFKA_GROUP_ID = os.environ.get('KAFKA_GROUP_ID', 'external_page_payment_proxy')
FASTAPI_BASE_URL = os.getenv('FASTAPI_BASE_URL')

kafka = Kafka(
    topic=KAFKA_TOPIC_NAME,
    bootstrap_servers=KAFKA_BOOTSTRAP_SERVERS,
    group_id=KAFKA_GROUP_ID,
    enable_auto_commit=True,
    seek_to_beginning=False,
    app_id=f'external-page-payment-proxy:{app_version}',
)


@asynccontextmanager
async def manage_startup_and_shutdown(app: FastAPI):
    logger.info('Initializing API ...')

    await kafka.initialize()
    await kafka.consume(external_page.process_payment)

    yield

    logger.info('Stopping consumer')
    logger.info('Stopping application')

    kafka.consumer_task.cancel()
    await kafka.consumer.stop()


# instantiate the API
app = FastAPI(
    title='External Page Proxy',
    version=app_version,
    openapi_url=f'{FASTAPI_BASE_URL or ""}/openapi.json',
    lifespan=manage_startup_and_shutdown,
)


@app.get('/status', tags=['Status'])
async def check_status():
    try:
        return {'status': 'Everything OK!'}
    except Exception:
        raise HTTPException(status_code=status.HTTP_503_SERVICE_UNAVAILABLE)


# return HTML response
@app.get('/online-payment/{payment_id}', tags=['Payment'])
async def get_online_url(payment_id: str = Path(example='5f99646d-769e-4d73-ba28-cffd30a77e74')):  # noqa: B008
    try:
        payment_url, payment, is_paid, landing_url = external_page.get_online_url(payment_id=payment_id)
        payment_page = get_template(
            reason=payment['reason'],
            amount=payment['payment']['amount'],
            payment_link=payment_url,
            landing_url=landing_url,
            payment_schema_url=f"{EXTERNAL_API_URL}{app.url_path_for('get_payment_form_schema')}",
            is_paid=is_paid,
        )
        await kafka.send_event(payment)
        return HTMLResponse(payment_page)
    except ExternalPageException:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Item not found')


@app.get('/receipt/{payment_id}', tags=['Payment'])
async def get_receipt_file(payment_id: str = Path(example='5f99646d-769e-4d73-ba28-cffd30a77e74')):  # noqa: B008
    try:
        receipt_pdf, payment = external_page.get_receipt_file(payment_id=payment_id)
        # await kafka.send_event(payment)
        if receipt_pdf:
            return Response(receipt_pdf, media_type='application/pdf')
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Receipt not available')
    except ExternalPageException:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Item not found')


@app.post('/landing/{payment_id}', tags=['Payment'])
async def get_landing_url(
    payment_id: str = Path(example='5f99646d-769e-4d73-ba28-cffd30a77e74'),  # noqa: B008
    submission: PaymentSummaryModel = Body(  # noqa: B008
        example={
            'data': {
                'iuv': '550000000019773',
                'receipt': [
                    {
                        'hash': '11aece5092dc5e670a82c0983fd98a95',
                        'name': 'example.pdf',
                        'originalName': 'example.pdf',
                        'size': 92207,
                        'storage': 'base64',
                        'type': 'application/pdf',
                        'url': 'data:application/pdf;base64,JVBERi0xLjc',
                    },
                ],
            },
        },
    ),
):
    try:
        redirect_url, payment = external_page.get_landing_url(payment_id=payment_id, payment_data=submission)
        await kafka.send_event(payment)
        return {'return_url': redirect_url}
    except ExternalPageException:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Item not found')
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)


@app.get('/payment-schema', tags=['Payment'])
async def get_payment_form_schema():  # noqa: B008
    try:
        return Response(get_payment_schema(), media_type='application/json')
    except SchemaNotFound as ce:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(ce))


@app.get('/tenants/schema', tags=['Tenants'])
async def get_tenant_form_schema():
    try:
        return Response(get_tenant_schema(), media_type='application/json')
    except SchemaNotFound as ce:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(ce))


@app.get('/tenants/{tenant_id}', response_model=Tenant, tags=['Tenants'])
async def get_tenant_configuration(
    tenant_id: UUID = Path(example=UUID('b212c4b4-db26-4404-8c7c-47dab99dd2e6')),  # noqa: B008
):
    try:
        tc = TenantConfigurator(tenant_id=tenant_id)
        return tc.get_configuration()
    except ConfigurationNotFound as ce:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(ce))
    except ValidationError as e:
        raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=str(e))


@app.post('/tenants', status_code=status.HTTP_201_CREATED, tags=['Tenants'])
async def save_tenant_configuration(
    configuration: Tenant = Body(example={'id': 'b212c4b4-db26-4404-8c7c-47dab99dd2e6', 'active': True}),  # noqa: B008
):
    try:
        tc = TenantConfigurator(tenant_id=configuration.id)
        tc.update_or_save_configuration(tenant=configuration, update=False)
        external_page.add_or_update_tenant_conf(configuration.to_dict())
        logger.debug(f'Updated tenant conf dict: {external_page.tenants}')
        return Response(status_code=status.HTTP_201_CREATED, media_type='application/json')
    except ConfigurationNotFound as ce:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(ce))
    except ConfigurationNotAllowed as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))


@app.put('/tenants/{tenant_id}', tags=['Tenants'])
async def update_tenant_configuration(
    configuration: TenantUpdate,
    tenant_id: UUID = Path(example=UUID('b212c4b4-db26-4404-8c7c-47dab99dd2e6')),  # noqa: B008
):
    try:
        tc = TenantConfigurator(tenant_id=tenant_id)
        config_data = Tenant(**configuration.to_dict(), id=tenant_id)
        tc.update_or_save_configuration(tenant=config_data, update=True)
        external_page.add_or_update_tenant_conf(config_data.to_dict())
        logger.debug(f'Updated tenant conf dict: {external_page.tenants}')
        return Response(status_code=status.HTTP_200_OK, media_type='application/json')
    except ConfigurationNotFound as ce:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(ce))
    except ConfigurationNotAllowed as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))


@app.patch('/tenants/{tenant_id}', tags=['Tenants'])
async def update_existing_tenant_configuration(
    tenant_id: UUID = Path(example=UUID('b212c4b4-db26-4404-8c7c-47dab99dd2e6')),  # noqa: B008
    new_configuration: dict = Body(example={'id': 'b212c4b4-db26-4404-8c7c-47dab99dd2e6'}),  # noqa: B008
):
    try:
        tc = TenantConfigurator(tenant_id=tenant_id)
        config_data = tc.update_existing_configuration(new_configuration=new_configuration)
        external_page.add_or_update_tenant_conf(config_data)
        logger.debug(f'Updated tenant conf dict: {external_page.tenants}')
        return Response(status_code=status.HTTP_200_OK, media_type='application/json')
    except ConfigurationNotFound as ce:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(ce))
    except ConfigurationNotAllowed as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))


@app.delete('/tenants/{tenant_id}', status_code=status.HTTP_204_NO_CONTENT, tags=['Tenants'])
async def delete_tenant(tenant_id: UUID = Path(example=UUID('b212c4b4-db26-4404-8c7c-47dab99dd2e6'))):  # noqa: B008
    try:
        tc = TenantConfigurator(tenant_id=tenant_id)
        tc.disable_configuration()
        external_page.delete_tenant_conf(tenant_id)
        logger.debug(f'Updated tenant conf dict: {external_page.tenants}')
        return Response(status_code=status.HTTP_204_NO_CONTENT)
    except (ConfigurationNotFound, KeyError) as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(e))
    except ConfigurationNotAllowed as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))


# API service configuration


@app.get('/services/schema', tags=['Services'])
async def get_service_form_schema():
    try:
        return Response(get_service_schema(), media_type='application/json')
    except SchemaNotFound as ce:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(ce))


@app.get('/services/{service_id}', response_model=Service, tags=['Services'])
async def get_service_configuration(
    service_id: UUID = Path(example='23d57b65-5eb9-4f0a-a507-fbcf3057b248'),  # noqa: B008
):
    try:
        sc = ServiceConfigurator(
            tenant_id=UUID(external_page.services[str(service_id)]['tenant_id'])
            if str(service_id) in external_page.services
            else None,
            service_id=service_id,
        )
        return sc.get_configuration()
    except ConfigurationNotFound as ce:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(ce))
    except ValidationError as e:
        raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=str(e))


@app.post('/services', status_code=status.HTTP_201_CREATED, tags=['Services'])
async def save_service_configuration(
    configuration: Service = Body(  # noqa: B008
        example={
            'tenant_id': 'b212c4b4-db26-4404-8c7c-47dab99dd2e6',
            'payment_link': 'https://payment-gateway.com/payment',
            'category': '9/0101100IM/',
            'reason': 'Pagamento Pratiche Polizia Municipale',
            'id': '23d57b65-5eb9-4f0a-a507-fbcf3057b248',
            'active': True,
        },
    ),
):
    try:
        if not str(configuration.tenant_id) in external_page.tenants:
            raise ConfigurationNotFound(f"Tenant {configuration.tenant_id} doesn't exist")
        sc = ServiceConfigurator(tenant_id=configuration.tenant_id, service_id=configuration.id)
        sc.update_or_save_configuration(service=configuration, update=False)
        external_page.add_or_update_service_conf(configuration.to_dict())
        logger.debug(f'Updated services conf dict: {external_page.services}')
        return Response(status_code=status.HTTP_201_CREATED, media_type='application/json')
    except ConfigurationNotFound as ce:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(ce))
    except ConfigurationNotAllowed as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))


@app.put('/services/{service_id}', tags=['Services'])
async def update_service_configuration(
    configuration: ServiceUpdate,
    service_id: UUID = Path(example='23d57b65-5eb9-4f0a-a507-fbcf3057b248'),  # noqa: B008
):
    try:
        if not str(configuration.tenant_id) in external_page.tenants:
            raise ConfigurationNotFound(f"Tenant {configuration.tenant_id} doesn't exist")
        sc = ServiceConfigurator(tenant_id=configuration.tenant_id, service_id=service_id)
        config_data = Service(**configuration.to_dict(), id=service_id)
        sc.update_or_save_configuration(service=config_data, update=True)
        external_page.add_or_update_service_conf(config_data.to_dict())
        logger.debug(f'Updated services conf dict: {external_page.services}')
        return Response(status_code=status.HTTP_200_OK, media_type='application/json')
    except ConfigurationNotFound as ce:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(ce))
    except ConfigurationNotAllowed as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))


@app.patch('/services/{service_id}', tags=['Services'])
async def update_existing_service_configuration(
    service_id: UUID = Path(example='23d57b65-5eb9-4f0a-a507-fbcf3057b248'),  # noqa: B008
    new_configuration: dict = Body(example={'payment_link': 'https://payment-gateway.com/payment'}),  # noqa: B008
):
    try:
        sc = ServiceConfigurator(
            tenant_id=UUID(external_page.services[str(service_id)]['tenant_id'])
            if str(service_id) in external_page.services
            else None,
            service_id=service_id,
        )
        config_data = sc.update_existing_configuration(new_configuration=new_configuration)
        external_page.add_or_update_service_conf(config_data)
        logger.debug(f'Updated services conf dict: {external_page.services}')
        return Response(status_code=status.HTTP_200_OK, media_type='application/json')
    except ConfigurationNotFound as ce:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(ce))
    except ConfigurationNotAllowed as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))


@app.delete('/services/{service_id}', status_code=status.HTTP_204_NO_CONTENT, tags=['Services'])
async def delete_service(service_id: UUID = Path(example='23d57b65-5eb9-4f0a-a507-fbcf3057b248')):  # noqa: B008
    try:
        sc = ServiceConfigurator(
            tenant_id=UUID(external_page.services[str(service_id)]['tenant_id'])
            if str(service_id) in external_page.services
            else None,
            service_id=service_id,
        )
        sc.disable_configuration()
        external_page.delete_service_conf(service_id)
        logger.debug(f'Updated service conf dict: {external_page.services}')
        return Response(status_code=status.HTTP_204_NO_CONTENT)
    except (ConfigurationNotFound, KeyError) as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(e))
    except ConfigurationNotAllowed as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))


external_page = ExternalPage(
    config_path=EXTERNAL_PAGE_CONFIG_PATH,
    app_online_url=f"{EXTERNAL_API_URL}{app.url_path_for('get_online_url', payment_id='{id}')}",
    app_receipt_url=f"{EXTERNAL_API_URL}{app.url_path_for('get_receipt_file', payment_id='{id}')}",
    app_landing_url=f"{EXTERNAL_API_URL}{app.url_path_for('get_landing_url', payment_id='{id}')}",
)


if __name__ == '__main__':
    uvicorn.run(
        app,
        host=APP_HOST,
        port=APP_PORT,
        proxy_headers=True,
        forwarded_allow_ips='*',
        access_log=False,
    )
