import os

from jinja2 import Environment, PackageLoader, select_autoescape

TEMPLATE_PACKAGE_NAME = os.environ.get('TEMPLATE_PACKAGE_NAME', 'src')

env = Environment(
    loader=PackageLoader(TEMPLATE_PACKAGE_NAME, 'templates'),
    autoescape=select_autoescape(['html', 'xml']),
)


def get_template(reason, amount, payment_link, landing_url, payment_schema_url, is_paid):
    template = env.get_template('payment_page.html')

    html = template.render(
        reason=reason,
        amount=amount,
        payment_link=payment_link,
        landing_url=landing_url,
        payment_schema_url=payment_schema_url,
        is_paid=is_paid,
    )

    return html
